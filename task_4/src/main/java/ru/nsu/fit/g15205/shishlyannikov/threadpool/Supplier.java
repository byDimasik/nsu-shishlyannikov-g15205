package ru.nsu.fit.g15205.shishlyannikov.threadpool;

import ru.nsu.fit.g15205.shishlyannikov.factory.stores.Store;

public abstract class Supplier implements Runnable {
    protected Store store;
    protected int lastID = 0;
    protected int supplierID;
    private int waitTime;

    public Supplier(Store store, int id, int time) {
        this.store = store;
        supplierID = id;
        waitTime = time;
    }

    abstract void supply() throws InterruptedException;

    public int getNumReadyParts() {
        return lastID;
    }

    public void setWaitTime(int time) {
        waitTime = time;
    }
    public int getWaitTime() {
        return waitTime;
    }

    @Override
    public void run() {
        while (true) {
            try {
                supply();
                Thread.sleep(waitTime);
            } catch (InterruptedException ex) {
                return;
            }
        }
    }
}
