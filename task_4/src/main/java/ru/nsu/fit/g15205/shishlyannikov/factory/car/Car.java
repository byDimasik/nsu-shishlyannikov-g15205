package ru.nsu.fit.g15205.shishlyannikov.factory.car;

public class Car {
    private CarPart accessories;
    private CarPart body;
    private CarPart engine;
    private int id;

    public Car(CarPart b, CarPart e, CarPart a, int id) {
        body = b;
        engine = e;
        accessories = a;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int[] getPartsId() {
        int[] ids = {accessories.getId(), body.getId(), engine.getId()};
        return ids;
    }
}
