package ru.nsu.fit.g15205.shishlyannikov.factory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/*
StoreBodySize=100
        StoreEngineSize=100
        StoreAccessoriesSize=100
        StoreEndProductSize=100
        AccessoriesSuppliers=5
        Workers=10
        Dealers=20
        LogSale=true */

public class FactoryConfigParser {
    private Properties properties = new Properties();

    public FactoryConfigParser() {
        try (InputStream inputStream = new FileInputStream("resources/factory.properties")){
            properties.load(inputStream);
        } catch (IOException ex) {
            System.err.println(ex.getLocalizedMessage());
            System.exit(1);
        }
    }

    public int getStoreBodySize() {
        return Integer.parseInt(properties.getProperty("StoreBodySize"));
    }

    public int getStoreEngineSize() {
        return Integer.parseInt(properties.getProperty("StoreEngineSize"));
    }

    public int getStoreAccessoriesSize() {
        return Integer.parseInt(properties.getProperty("StoreAccessoriesSize"));
    }

    public int getStoreEndProductSize() {
        return Integer.parseInt(properties.getProperty("StoreEndProductSize"));
    }

    public int getAccessoriesSuppliersNum() {
        return Integer.parseInt(properties.getProperty("AccessoriesSuppliers"));
    }

    public int getWorkersNum() {
        return Integer.parseInt(properties.getProperty("Workers"));
    }

    public int getDealersNum() {
        return Integer.parseInt(properties.getProperty("Dealers"));
    }

    public boolean getLog() {
        return Boolean.parseBoolean(properties.getProperty("LogSale"));
    }
}
