package ru.nsu.fit.g15205.shishlyannikov.threadpool;

import ru.nsu.fit.g15205.shishlyannikov.factory.stores.EndProductStore;

import java.util.concurrent.BlockingQueue;

public class EndProductStoreController implements Runnable {
    private EndProductStore store;
    private BlockingQueue<Object> queue;
    private final Object storeChangeState;

    public EndProductStoreController(EndProductStore store, BlockingQueue<Object> queue, Object state) {
        this.store = store;
        this.queue = queue;
        storeChangeState = state;
    }

    @Override
    public void run() {
        while (true) {
            try {
                int num = store.getFreeSpace();
                if (num != 0) {
                    for (int i = 0; i < num; i++) {
                        queue.add(new Object());
                    }
                }
                synchronized (storeChangeState) {
                    storeChangeState.wait();
                }
            } catch (InterruptedException ex) {
                queue.clear();
                return;
            }
        }
    }
}
