package ru.nsu.fit.g15205.shishlyannikov.factory.car;

public class CarPart {
    private int id;

    public CarPart(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
