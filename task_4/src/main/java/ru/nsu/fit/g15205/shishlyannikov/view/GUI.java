package ru.nsu.fit.g15205.shishlyannikov.view;

import ru.nsu.fit.g15205.shishlyannikov.factory.stores.EndProductStore;
import ru.nsu.fit.g15205.shishlyannikov.factory.stores.Store;
import ru.nsu.fit.g15205.shishlyannikov.threadpool.AccessoriesSupplier;
import ru.nsu.fit.g15205.shishlyannikov.threadpool.BodySupplier;
import ru.nsu.fit.g15205.shishlyannikov.threadpool.Dealer;
import ru.nsu.fit.g15205.shishlyannikov.threadpool.EngineSupplier;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

public class GUI extends JFrame {
    private Store accessoriesStore;
    private Store bodiesStore;
    private Store enginesStore;
    private EndProductStore endProductStore;

    private AccessoriesSupplier[] accessoriesSuppliers;
    private BodySupplier bodySupplier;
    private EngineSupplier engineSupplier;
    private Dealer[] dealers;

    private TextSlider accessorySlider = new TextSlider("Аксессуары:",100, 5000);
    private TextSlider bodySlider = new TextSlider("Корпуса:", 100, 5000);
    private TextSlider engineSlider = new TextSlider("Двигатели:",100, 5000);
    private TextSlider dealersSlider = new TextSlider("Дилеры:", 100, 5000);

    private JPanel infoPanel = new JPanel(new GridLayout(5, 3));
    private JLabel[] info = new JLabel[8];

    public GUI(Store a, Store b, Store e, AccessoriesSupplier[] as, BodySupplier bs, EngineSupplier es,
               EndProductStore end, Dealer[] d) {
        super("Factory");
        setPreferredSize(new Dimension(400, 500));
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        getContentPane().setLayout(new GridLayout(5, 1));
        getContentPane().add(infoPanel);

        accessoriesStore = a;
        bodiesStore = b;
        enginesStore = e;
        endProductStore = end;

        accessoriesSuppliers = as;
        bodySupplier = bs;
        engineSupplier = es;
        dealers = d;

        for (int i = 0; i < info.length; i++) {
            info[i] = new JLabel("");
        }

        accessorySlider.getSlider().setValue(accessoriesSuppliers[0].getWaitTime());
        bodySlider.getSlider().setValue(bodySupplier.getWaitTime());
        engineSlider.getSlider().setValue(engineSupplier.getWaitTime());
        dealersSlider.getSlider().setValue(dealers[0].getWaitTime());

        accessorySlider.getSlider().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                for (int i = 0; i < accessoriesSuppliers.length; i++) {
                    accessoriesSuppliers[i].setWaitTime(accessorySlider.getSlider().getValue());
                }
            }
        });
        getContentPane().add(accessorySlider.getPanel());

        bodySlider.getSlider().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                bodySupplier.setWaitTime(bodySlider.getSlider().getValue());
            }
        });
        getContentPane().add(bodySlider.getPanel());

        engineSlider.getSlider().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                engineSupplier.setWaitTime(engineSlider.getSlider().getValue());
            }
        });
        getContentPane().add(engineSlider.getPanel());

        dealersSlider.getSlider().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                for (Dealer dealer : dealers) {
                    dealer.setWaitTime(dealersSlider.getSlider().getValue());
                }
            }
        });
        getContentPane().add(dealersSlider.getPanel());
    }

    private void updateInfo() {
        infoPanel.removeAll();
        int accessoriesNum = 0, carsNum = 0;
        for (int i = 0; i < accessoriesSuppliers.length; i++) {
            accessoriesNum += accessoriesSuppliers[i].getNumReadyParts();
        }
        for (Dealer dealer: dealers) {
            carsNum += dealer.getCarsNum();
        }

        info[0].setText(String.valueOf(accessoriesStore.getStoreState()));
        info[1].setText(String.valueOf(accessoriesNum));
        info[2].setText(String.valueOf(bodiesStore.getStoreState()));
        info[3].setText(String.valueOf(bodySupplier.getNumReadyParts()));
        info[4].setText(String.valueOf(enginesStore.getStoreState()));
        info[5].setText(String.valueOf(engineSupplier.getNumReadyParts()));
        info[6].setText(String.valueOf(endProductStore.getStoreState()));
        info[7].setText(String.valueOf(carsNum));

        infoPanel.add(new JLabel(""));
        infoPanel.add(new JLabel("На складе"));
        infoPanel.add(new JLabel("Сделано всего"));

        infoPanel.add(new JLabel("Акссессуары:"));
        infoPanel.add(info[0]);
        infoPanel.add(info[1]);

        infoPanel.add(new JLabel("Корпуса:"));
        infoPanel.add(info[2]);
        infoPanel.add(info[3]);

        infoPanel.add(new JLabel("Двигатели:"));
        infoPanel.add(info[4]);
        infoPanel.add(info[5]);

        infoPanel.add(new JLabel("Машины:"));
        infoPanel.add(info[6]);
        infoPanel.add(info[7]);

    }

    public void updateFrame() {
        SwingUtilities.invokeLater(() -> {
            updateInfo();

            pack();
            repaint();
            setVisible(true);
        });
    }
}
