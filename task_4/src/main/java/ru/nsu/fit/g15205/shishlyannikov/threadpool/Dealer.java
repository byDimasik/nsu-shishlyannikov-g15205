package ru.nsu.fit.g15205.shishlyannikov.threadpool;

import org.apache.log4j.Logger;
import ru.nsu.fit.g15205.shishlyannikov.factory.car.Car;
import ru.nsu.fit.g15205.shishlyannikov.factory.stores.EndProductStore;

public class Dealer implements Runnable {
    private static final Logger logger = Logger.getLogger(Dealer.class);
    private EndProductStore endProductStore;
    private int dealerID;
    private int waitTime;
    private boolean log;
    private int carsNum = 0;

    public Dealer(EndProductStore store, int id, int time, boolean log) {
        endProductStore = store;
        dealerID = id;
        waitTime = time;
        this.log = log;
    }

    public void setWaitTime(int time) {
        waitTime = time;
    }
    public int getWaitTime() {
        return waitTime;
    }
    public int getCarsNum() {
        return carsNum;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Car car = endProductStore.getCar();
                carsNum++;
                if (log) {
                    int[] ids = car.getPartsId();
                    logger.info("Dealer " + dealerID + ": Car " + car.getId() +
                            " (Body: " + ids[1] + ", Motor: " + ids[2] + ", Accessory: " + ids[0] + ")");
                }
                //System.out.println("Dealer " + dealerID + " get car " + car.getId());
                Thread.sleep(waitTime);
            } catch (InterruptedException ex) {
                return;
            }
        }
    }
}
