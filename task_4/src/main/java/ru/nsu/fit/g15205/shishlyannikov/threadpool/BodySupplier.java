package ru.nsu.fit.g15205.shishlyannikov.threadpool;

import ru.nsu.fit.g15205.shishlyannikov.factory.car.Body;
import ru.nsu.fit.g15205.shishlyannikov.factory.stores.Store;

public class BodySupplier extends Supplier {
    public BodySupplier(Store store, int id, int time) {
        super(store, id, time);
    }

    public void supply() throws InterruptedException {
        int accessoryID = Integer.valueOf(String.valueOf(supplierID) + String.valueOf(lastID));
        lastID++;
        store.addCarPart(new Body(accessoryID));
    }
}
