package ru.nsu.fit.g15205.shishlyannikov.factory.stores;

import ru.nsu.fit.g15205.shishlyannikov.factory.car.Car;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class EndProductStore {
    private ArrayList<Car> cars = new ArrayList<>();
    private int size;
    private Lock lock = new ReentrantLock();
    private Condition full = lock.newCondition();
    private Condition empty = lock.newCondition();
    private final Object storeChangeState;

    public EndProductStore(int size, Object state) {
        this.size = size;
        storeChangeState = state;
    }

    public void addCar(Car car) throws InterruptedException {
        lock.lock();

        try {
            while (cars.size() >= size) {
                full.await();
            }
            cars.add(car);
            empty.signalAll();
        } catch (InterruptedException ex) {
            lock.unlock();
            throw ex;
        }
        lock.unlock();
    }

    public Car getCar() throws InterruptedException {
        lock.lock();

        Car car;
        try {
            while (cars.size() == 0) {
                empty.await();
            }
            car = cars.remove(0);
            full.signalAll();

            synchronized (storeChangeState) {
                storeChangeState.notifyAll();
            }
        } catch (InterruptedException ex) {
            lock.unlock();
            throw ex;
        }

        lock.unlock();

        return car;
    }

    public int getFreeSpace() {
        lock.lock();

        int freeSpace = size - cars.size();

        lock.unlock();

        return freeSpace;
    }

    public int getStoreState() {
        lock.lock();
        int state =  cars.size();
        lock.unlock();

        return state;
    }
}
