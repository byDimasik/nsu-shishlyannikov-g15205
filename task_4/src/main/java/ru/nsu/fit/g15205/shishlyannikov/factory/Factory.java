package ru.nsu.fit.g15205.shishlyannikov.factory;

import ru.nsu.fit.g15205.shishlyannikov.factory.stores.EndProductStore;
import ru.nsu.fit.g15205.shishlyannikov.factory.stores.Store;
import ru.nsu.fit.g15205.shishlyannikov.threadpool.*;
import ru.nsu.fit.g15205.shishlyannikov.view.GUI;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class Factory {
    private FactoryConfigParser config;
    private Thread[] accessoriesSuppliersThreads;
    private Thread bodiesSupplierThread;
    private Thread enginesSupplierThread;
    private Thread endProductStoreControllerThread;
    private Thread[] workers;
    private Thread[] dealersThreads;
    private boolean processing = true;

    public Factory(FactoryConfigParser p) {
        config = p;
    }

    public void start() {
        Store accessoriesStore = new Store(config.getStoreAccessoriesSize());
        Store bodiesStore = new Store(config.getStoreBodySize());
        Store enginesStore = new Store(config.getStoreEngineSize());

        accessoriesSuppliersThreads = new Thread[config.getAccessoriesSuppliersNum()];
        AccessoriesSupplier[] accessoriesSuppliers = new AccessoriesSupplier[config.getAccessoriesSuppliersNum()];
        for (int i = 0; i < accessoriesSuppliersThreads.length; i++) {
            accessoriesSuppliers[i] = new AccessoriesSupplier(accessoriesStore, i, 200);
            accessoriesSuppliersThreads[i] = new Thread(accessoriesSuppliers[i], "Accessory Supplier " + i);
            accessoriesSuppliersThreads[i].start();
        }

        BodySupplier bodySupplier = new BodySupplier(bodiesStore, 1, 400);

        bodiesSupplierThread = new Thread(bodySupplier, "Bodies Supplier");
        bodiesSupplierThread.start();

        EngineSupplier engineSupplier = new EngineSupplier(enginesStore, 1, 500);
        enginesSupplierThread = new Thread(engineSupplier, "Engines Supplier");
        enginesSupplierThread.start();

        Object changeState = new Object();
        EndProductStore endProductStore = new EndProductStore(config.getStoreEndProductSize(), changeState);

        BlockingQueue<Object> queue = new LinkedBlockingDeque<>();
        Runnable endProductStoreController = new EndProductStoreController(endProductStore, queue, changeState);
        endProductStoreControllerThread = new Thread(endProductStoreController, "End Product Store Controller");
        endProductStoreControllerThread.start();

        workers = new Thread[config.getWorkersNum()];
        for (int i = 0; i < workers.length; i++) {
            Runnable worker = new Worker(accessoriesStore, bodiesStore, enginesStore, endProductStore, i, queue);
            workers[i] = new Thread(worker, "Worker " + i);
            workers[i].start();
        }

        dealersThreads = new Thread[config.getDealersNum()];
        Dealer[] dealers = new Dealer[config.getDealersNum()];
        for (int i = 0; i < dealersThreads.length; i++) {
            dealers[i] = new Dealer(endProductStore, i, 100, config.getLog());
            dealersThreads[i] = new Thread(dealers[i], "Dealer " + i);
            dealersThreads[i].start();
        }

        GUI gui = new GUI(accessoriesStore, bodiesStore, enginesStore,
                accessoriesSuppliers, bodySupplier, engineSupplier, endProductStore, dealers);
        gui.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                close();
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });

        while (processing) {
            try {
                gui.updateFrame();
                Thread.sleep(24);
            } catch (InterruptedException ex) {
                return;
            }
        }
    }

    private void close() {
        for (Thread supplierThread : accessoriesSuppliersThreads) {
            supplierThread.interrupt();
        }

        bodiesSupplierThread.interrupt();
        enginesSupplierThread.interrupt();
        endProductStoreControllerThread.interrupt();

        for (Thread workerThread : workers) {
            workerThread.interrupt();
        }

        for (Thread dealerThread : dealersThreads) {
            dealerThread.interrupt();
        }
        processing = false;
    }
}
