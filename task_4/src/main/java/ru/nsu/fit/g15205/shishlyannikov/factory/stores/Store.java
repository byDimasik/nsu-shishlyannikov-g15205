package ru.nsu.fit.g15205.shishlyannikov.factory.stores;

import ru.nsu.fit.g15205.shishlyannikov.factory.car.CarPart;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Store {
    private ArrayList<CarPart> carParts = new ArrayList<>();
    private int size;
    private Lock lock = new ReentrantLock();
    private Condition full = lock.newCondition();
    private Condition empty = lock.newCondition();

    public Store(int size) {
        this.size = size;
    }

    public void addCarPart(CarPart part) throws InterruptedException {
        lock.lock();

        try {
            while (carParts.size() == size) {
                full.await();
            }
            carParts.add(part);
            empty.signalAll();
        } catch (InterruptedException ex) {
            lock.unlock();
            throw ex;
        }

        lock.unlock();
    }

    public CarPart getCarPart() throws InterruptedException {
        lock.lock();

        CarPart carPart;
        try {
            while (carParts.size() == 0) {
                empty.await();
            }
            carPart = carParts.remove(0);
            full.signalAll();
        } catch (InterruptedException ex) {
            lock.unlock();
            throw ex;
        }

        lock.unlock();

        return carPart;
    }

    public int getStoreState() {
        lock.lock();
        int state =  carParts.size();
        lock.unlock();

        return state;
    }
}
