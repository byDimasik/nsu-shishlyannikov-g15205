package ru.nsu.fit.g15205.shishlyannikov;

import ru.nsu.fit.g15205.shishlyannikov.factory.Factory;
import ru.nsu.fit.g15205.shishlyannikov.factory.FactoryConfigParser;

import java.io.IOException;


public class App
{
    public static void main( String[] args ) {
        FactoryConfigParser config = new FactoryConfigParser();

        Factory factory = new Factory(config);
        factory.start();
    }
}
