package ru.nsu.fit.g15205.shishlyannikov.view;


import javax.swing.*;
import java.awt.*;

public class TextSlider {
    private JSlider slider;
    private JLabel label;
    private JPanel panel;

    public TextSlider(String text, int start, int end) {
        slider = new JSlider(start, end);
        slider.setMajorTickSpacing(980);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);

        label = new JLabel(text);

        panel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        panel.add(label, c);

        c.gridx = 1;
        panel.add(slider, c);
    }

    public JSlider getSlider() {
        return slider;
    }
    public JPanel getPanel() {
        return panel;
    }
}
