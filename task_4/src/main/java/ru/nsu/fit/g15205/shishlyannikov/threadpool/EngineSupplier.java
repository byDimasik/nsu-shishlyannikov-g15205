package ru.nsu.fit.g15205.shishlyannikov.threadpool;

import ru.nsu.fit.g15205.shishlyannikov.factory.car.Engine;
import ru.nsu.fit.g15205.shishlyannikov.factory.stores.Store;

public class EngineSupplier extends Supplier {
    public EngineSupplier(Store store, int id, int time) {
        super(store, id, time);
    }

    public void supply() throws InterruptedException {
        int accessoryID = Integer.valueOf(String.valueOf(supplierID) + String.valueOf(lastID));
        lastID++;
        store.addCarPart(new Engine(accessoryID));
    }
}
