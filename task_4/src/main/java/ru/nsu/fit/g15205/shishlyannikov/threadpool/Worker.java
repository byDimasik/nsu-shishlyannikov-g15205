package ru.nsu.fit.g15205.shishlyannikov.threadpool;

import ru.nsu.fit.g15205.shishlyannikov.factory.car.*;
import ru.nsu.fit.g15205.shishlyannikov.factory.stores.EndProductStore;
import ru.nsu.fit.g15205.shishlyannikov.factory.stores.Store;

import java.util.concurrent.BlockingQueue;

public class Worker implements Runnable {
    private Store accessoriesStore;
    private Store bodiesStore;
    private Store enginesStore;
    private EndProductStore endProductStore;
    private int workerID;
    private int lastCarID = 0;
    private BlockingQueue<Object> queue;

    public Worker(Store accessoriesStore, Store bodiesStore, Store enginesStore, EndProductStore endProductStore, int id,
                  BlockingQueue<Object> queue) {
        this.accessoriesStore = accessoriesStore;
        this.bodiesStore = bodiesStore;
        this.enginesStore = enginesStore;
        this.endProductStore = endProductStore;
        this.queue = queue;
        workerID = id;
    }

    public void buildCar() throws InterruptedException {
        queue.take();
        CarPart body = bodiesStore.getCarPart();
        CarPart engine = enginesStore.getCarPart();
        CarPart accessories = accessoriesStore.getCarPart();
        int carID = Integer.valueOf(String.valueOf(workerID) + String.valueOf(lastCarID));
        lastCarID++;
        endProductStore.addCar(new Car(body, engine, accessories, carID));
    }

    @Override
    public void run() {
        while (true) {
            try {
                buildCar();
            } catch (InterruptedException ex) {
                queue.clear();
                return;
            }
        }
    }
}
