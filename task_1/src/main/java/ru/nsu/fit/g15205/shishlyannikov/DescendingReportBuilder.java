package ru.nsu.fit.g15205.shishlyannikov;

import java.util.*;

public class DescendingReportBuilder implements ReportBuilder {
    private List<Map.Entry<String, WordCounter>> list; // упорядоченный список для репорта

    public DescendingReportBuilder(Map<String, WordCounter> map) {
        list = new ArrayList<>(map.entrySet());
    }

    // подготовка репорта
    // принимает лист, который будет сформирован в файл
    // возвращает общее количество слов
    public int buildReport(List<Map.Entry<String, WordCounter>> res) {
        Collections.sort(list, Map.Entry.comparingByValue(new Comparator<WordCounter>() {
            @Override
            public int compare(WordCounter lvalue, WordCounter rvalue) {
                return rvalue.getCount() - lvalue.getCount();
            }
        })); // сортируем мапу из WordProcessor

        int all_words = 0; // счетчик общего количества слов
        for (Map.Entry<String, WordCounter> i : list) {
            res.add(i); // формируем конечный лист
            all_words += i.getValue().getCount(); // считаем слова
        }

        return all_words;
    }
}
