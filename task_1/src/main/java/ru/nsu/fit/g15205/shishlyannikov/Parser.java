package ru.nsu.fit.g15205.shishlyannikov;

public interface Parser {
    String getWord();
}
