package ru.nsu.fit.g15205.shishlyannikov;

import java.util.*;

// Счетчик частоты слов в файле
public class WordProcessor {
    private Parser parser;
    private Map<String, WordCounter> map = new TreeMap<>();

    public WordProcessor(Parser p) {
        parser = p;

        countFreq();
    }

    // считаем частоту
    public void countFreq() {
        String str;

        while (true) {
            str = parser.getWord(); // получаем слово из файла

            if (str == null)
                break;

            WordCounter wc = map.get(str);
            if (wc == null) { // если слова еще нет в таблице, то добавляем
                map.put(str, new WordCounter());
            } // иначе инкрементируем значение по этому слову
            else
                wc.inc();
        }
    }

    public Map<String, WordCounter> getMap() {
        return map;
    }

}
