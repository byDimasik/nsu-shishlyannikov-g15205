package ru.nsu.fit.g15205.shishlyannikov;

import java.io.IOException;
import java.io.Reader;

public class TXTParser implements Parser {
    private Reader reader = null;
    private StringBuilder str;
    private String word;

    public TXTParser(Reader r) {
        reader = r;
        str = new StringBuilder(); // буфер, в котором будем собирать строку
    }

    // получить слово из файла
    public String getWord() {
        int charFromReader;
        try {
            while (true) {
                if ((charFromReader = reader.read()) < 0)
                    break;

                if (Character.isLetterOrDigit(charFromReader)) {
                    str.append((char) charFromReader);
                    break;
                }
            }

            // считываем символы, пока не встретим разделитель или конец файла
            while ((charFromReader = reader.read()) >= 0 && Character.isLetterOrDigit(charFromReader)) {
                str.append((char) charFromReader);
            }

            if (charFromReader >= 0 || str.length() != 0) {
                word = str.toString();
                str.delete(0, str.length());
                return word;
            }

            if (charFromReader < 0)
                reader.close();

        } catch (IOException ioex) {
            System.err.println("Error: " + ioex);
        }

        return null;
    }

}
