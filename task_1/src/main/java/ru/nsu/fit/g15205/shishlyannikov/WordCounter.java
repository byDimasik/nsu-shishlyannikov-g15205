package ru.nsu.fit.g15205.shishlyannikov;

import java.util.Comparator;

// Счетчик с инкрементом
public class WordCounter {
    private int count = 1;

    public void inc() { count++; }
    public int getCount() { return count; }
}
