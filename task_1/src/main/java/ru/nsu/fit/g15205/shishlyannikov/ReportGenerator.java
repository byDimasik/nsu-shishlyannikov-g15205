package ru.nsu.fit.g15205.shishlyannikov;


public interface ReportGenerator {
    void makeReport(String file_out_name);
}
