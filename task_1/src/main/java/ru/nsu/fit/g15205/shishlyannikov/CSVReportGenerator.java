package ru.nsu.fit.g15205.shishlyannikov;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CSVReportGenerator implements ReportGenerator {
    private ReportBuilder reportBuilder;

    public CSVReportGenerator(ReportBuilder rb) { reportBuilder = rb; }

    public void makeReport(String file_out_name) {
        List<Map.Entry<String, WordCounter>> list = new ArrayList<>();
        int all_words = reportBuilder.buildReport(list); // получаем лист с данными и количество слов

        try (FileWriter writer = new FileWriter(file_out_name)) { // создаем выходной файл
            for (Map.Entry<String, WordCounter> i : list) {
                // частота в процентах с двумя знаками после запятой
                String freq = String.format(Locale.US, "%.02f", (float)i.getValue().getCount()*100/all_words);

                // вывод: слово, частота, частота в процентах
                writer.write(i.getKey() + "," + i.getValue().getCount() + "," + freq + "\n");
            }

            writer.flush();

        } catch (IOException ioex) {
            System.err.println("Error while reading file: " + ioex.getLocalizedMessage());
        }


    }
}
