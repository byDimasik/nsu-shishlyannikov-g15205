package ru.nsu.fit.g15205.shishlyannikov;

import java.io.*;

public class App 
{
    public static void main( String[] args )
    {
        if (args.length < 2) {
            System.err.println("Error: недостаточно аргументов");
            return;
        }
        try (Reader reader = new InputStreamReader(new FileInputStream(args[0])))
        {
            Parser parser = new TXTParser(reader);

            WordProcessor wordProcessor = new WordProcessor(parser);

            ReportBuilder reportBuilder = new DescendingReportBuilder(wordProcessor.getMap());

            ReportGenerator reportGenerator = new CSVReportGenerator(reportBuilder);
            reportGenerator.makeReport(args[1]);
        }
        catch (IOException e)
        {
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }


    }
}
