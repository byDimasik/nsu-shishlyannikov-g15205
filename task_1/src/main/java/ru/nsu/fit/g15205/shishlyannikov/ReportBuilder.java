package ru.nsu.fit.g15205.shishlyannikov;

import java.util.*;

// Подготавливаем данные для репорта
public interface ReportBuilder {
    int buildReport(List<Map.Entry<String, WordCounter>> res);
}
