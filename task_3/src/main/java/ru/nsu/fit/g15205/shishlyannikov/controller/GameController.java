package ru.nsu.fit.g15205.shishlyannikov.controller;

import ru.nsu.fit.g15205.shishlyannikov.model.Garden;
import ru.nsu.fit.g15205.shishlyannikov.view.GameFrame;
import ru.nsu.fit.g15205.shishlyannikov.view.GameView;
import ru.nsu.fit.g15205.shishlyannikov.view.MainMenu;

public class GameController {
    private Garden garden;
    private MainMenu mainMenu;
    private GameFrame frame;
    private GameView gameView;

    public GameController(GameFrame frame, Garden garden, MainMenu mainMenu) {
        this.frame = frame;
        this.garden = garden;
        this.mainMenu = mainMenu;
    }
    public void addGameView(GameView gameView) {
        this.gameView = gameView;
    }
    public void backToMenu() {
        frame.updateFrame(mainMenu.backToMenu());
    }
    public void updateSeason() {
        frame.updateFrame(gameView.getGameView());
    }
    public void about() { frame.updateFrame(mainMenu.aboutTrees());}
    public void nextSeason() {
        garden.nextSeason();
        updateSeason();
    }

}

