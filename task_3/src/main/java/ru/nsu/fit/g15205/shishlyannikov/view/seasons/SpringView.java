package ru.nsu.fit.g15205.shishlyannikov.view.seasons;

import ru.nsu.fit.g15205.shishlyannikov.controller.GameController;
import ru.nsu.fit.g15205.shishlyannikov.controller.SpringController;
import ru.nsu.fit.g15205.shishlyannikov.model.*;
import ru.nsu.fit.g15205.shishlyannikov.view.GameButton;
import ru.nsu.fit.g15205.shishlyannikov.view.SeasonView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.TreeMap;

public class SpringView implements SeasonView {
    private JPanel jPanel;
    private JSlider slider;
    private SpringController springController;
    private Garden garden;
    private GameController gameController;

    public SpringView(SpringController springController, Garden garden, GameController gameController) {
        this.garden = garden;
        this.springController = springController;
        this.gameController = gameController;
    }

    public JPanel seasonComing() {
        Map<Tree, TreesCounter> trees = garden.getNumberOfTrees();

        jPanel = new JPanel(new GridLayout(trees.keySet().size()+1, 2));
        jPanel.setBackground(new Color(249, 223, 252));

        slider = new JSlider(0, garden.getFreeSpace());
        slider.setValue(1);
        slider.setMinorTickSpacing(1);
        slider.setMajorTickSpacing(5);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);

        JLabel treeLabel;
        JButton button;
        for (Tree tree : trees.keySet()) {
            treeLabel = new JLabel(tree.getTreeName() + ": " + trees.get(tree).getCount());
            button = new GameButton("Посадить!", "spring");
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    springController.plant(tree, slider.getValue());

                    gameController.updateSeason();
                }
            });

            jPanel.add(treeLabel);
            jPanel.add(button);
        }
        jPanel.add(slider);

        return jPanel;
    }

    public Color getColor() {
        return new Color(249, 223, 252);
    }
}
