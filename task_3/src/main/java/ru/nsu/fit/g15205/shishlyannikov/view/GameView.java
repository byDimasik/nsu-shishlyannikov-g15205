package ru.nsu.fit.g15205.shishlyannikov.view;

import ru.nsu.fit.g15205.shishlyannikov.controller.GameController;
import ru.nsu.fit.g15205.shishlyannikov.model.Garden;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameView {
    private Garden garden;
    private JPanel jPanel;

    public GameView(GameController gameController, Garden garden) {
        this.garden = garden;
        gameController.addGameView(this);
        jPanel = new JPanel(new GridLayout(1, 2));

        JButton button = new JButton("Меню");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gameController.backToMenu();
            }
        });
        jPanel.add(button);

        button = new JButton("Следующий сезон");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gameController.nextSeason();
            }
        });
        jPanel.add(button);
    }

    public JPanel getGameView() {
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        panel.setBackground(garden.getSeason().getColor());
        jPanel.setBackground(garden.getSeason().getColor());
        c.gridy = 1;
        c.weightx = c.weighty = 1;
        JPanel seasonPanel = garden.getSeason().seasonComing();
        seasonPanel.setPreferredSize(new Dimension(640, 430));
        panel.add(seasonPanel, c);
        c.gridy = 2;
        panel.add(jPanel, c);

        return panel;
    }
}
