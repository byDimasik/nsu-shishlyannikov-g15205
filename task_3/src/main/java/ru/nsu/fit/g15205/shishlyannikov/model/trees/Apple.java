package ru.nsu.fit.g15205.shishlyannikov.model.trees;

import ru.nsu.fit.g15205.shishlyannikov.model.Tree;

public class Apple extends Tree {
    public Apple() {
        super();
    }

    @Override
    public String getTreeType() {
        return "apple";
    }

    @Override
    public String getTreeName() {
        return "Яблоня";
    }
}
