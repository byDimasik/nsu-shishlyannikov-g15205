package ru.nsu.fit.g15205.shishlyannikov.view;

import ru.nsu.fit.g15205.shishlyannikov.controller.GameController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class MainMenu {
    private ImagePanel jPanel;
    private GameController gameController;

    public MainMenu() {
        try {

            GridBagConstraints c = new GridBagConstraints();

            Image image = ImageIO.read(new File("resources/images/mainMenu/back.jpg"));
            image = image.getScaledInstance(640, 500, Image.SCALE_DEFAULT);
            jPanel = new ImagePanel(image);

            //String[] images = {"resources/images/mainMenu/Par.png", "resources/images/mainMenu/Cursor.png", "resources/images/mainMenu/Click.png"};

            GameButton button1 = new GameButton("Да будет сад!", "mainMenu");
            GameButton button2 = new GameButton("Энциклопедия", "mainMenu");
            GameButton button4 = new GameButton("Выход", "mainMenu");


            button1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    gameController.updateSeason();
                }
            });
            button2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    gameController.about();
                }
            });
            button4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.exit(0);
                }
            });

            c.insets = new Insets(100, 410, 10, 1);
            c.gridy = 1;
            jPanel.add(button1, c);
            c.insets = new Insets(1, 410, 10, 1);
            c.gridy = 2;
            jPanel.add(button2, c);
            c.gridy = 3;
            jPanel.add(button4, c);

        } catch (IOException ex) {
            System.err.println(ex.getLocalizedMessage());
        }
    }

    public void addController(GameController gameController) {
        this.gameController = gameController;
    }
    public JPanel backToMenu() {
        return jPanel;
    }

    public JPanel aboutTrees() {
        JPanel jPanel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.ipadx = 300;
        jPanel.setBackground(new Color(212, 255, 202));

        JLabel jLabel = new JLabel("<html>В игре Веселый Садовник вы станете хозяином собственного сада.<br>" +
                "Садите деревья весной, но помните, что у вас есть только 50 мест в саду.<br>" +
                "Летом самое время удобрять деревья. Удобренное дерево дает на 20% больше урожая. Одно удобрение стоит 3000 р.<br>" +
                "Осенью продавайте выращенный урожай.<br>" +
                "Деревья не вечны. Старые деревья можно выкорчевать зимой.<br><br>" +
                "В игре есть 4 типа деревьев:<br><pre>" +
                "1. Яблони<br>" +
                "   Растут: 2 года.<br>" +
                "   Плодоносят: 10 лет.<br>" +
                "   Дают урожая: 45-60 кг.<br>" +
                "   Цена за кг урожая: 60 р.<br>" +
                "2. Вишни<br>" +
                "   Растут: 3 года.<br>" +
                "   Плодоносят: 8 лет.<br>" +
                "   Дают урожая: 40-60 кг.<br>" +
                "   Цена за кг урожая: 50 р.<br>" +
                "3. Персики<br>" +
                "   Растут: 6 лет.<br>" +
                "   Плодоносят: 16 лет.<br>" +
                "   Дают урожая: 5-25 кг.<br>" +
                "   Цена за кг урожая: 220 р.<br>" +
                "4. Груши<br>" +
                "   Растут: 2 года.<br>" +
                "   Плодоносят: 12 лет.<br>" +
                "   Дают урожая: 12-15 кг.<br>" +
                "   Цена за кг урожая: 100 р.</pre></html>");
        jPanel.add(jLabel, c);


        JButton backButton = new JButton("Назад");
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gameController.backToMenu();
            }
        });
        c.gridx = 1;
        c.ipadx = 5;
        jPanel.add(backButton, c);

        return jPanel;
    }

}
