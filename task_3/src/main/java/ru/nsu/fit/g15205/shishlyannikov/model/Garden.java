package ru.nsu.fit.g15205.shishlyannikov.model;

import ru.nsu.fit.g15205.shishlyannikov.controller.*;
import ru.nsu.fit.g15205.shishlyannikov.view.*;
import ru.nsu.fit.g15205.shishlyannikov.view.seasons.AutumnView;
import ru.nsu.fit.g15205.shishlyannikov.view.seasons.SpringView;
import ru.nsu.fit.g15205.shishlyannikov.view.seasons.SummerView;
import ru.nsu.fit.g15205.shishlyannikov.view.seasons.WinterView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Garden {
    private ArrayList<Tree> trees = new ArrayList<>();
    private Map<String, TreesCounter> numberOfTrees = new HashMap<>();
    private Map<Tree, TreesCounter> map = new HashMap<>();
    private TreesFactory factory = new TreesFactory();
    private final int gardenSize = 50;
    private int money = 0;
    private final int fertilizePrice = 5000;

    private SeasonView springView;
    private SeasonView summerView;
    private SeasonView autumnView;
    private SeasonView winterView;

    private SeasonView season = springView;

    public Garden() {
        for (Tree tree : factory.getAllTrees()) {
            numberOfTrees.put(tree.getTreeType(), new TreesCounter());
            map.put(tree, new TreesCounter());
        }
    }

    public void addGameController(GameController gameController) {
        springView = new SpringView(new SpringController(this), this, gameController);
        summerView = new SummerView(new SummerController(this), this, gameController);
        autumnView = new AutumnView(new AutumnController(this), this, gameController);
        winterView = new WinterView(new WinterController(this), this, gameController);

        season = springView;
    }

    public void addTrees(Tree tree, int num) {
        for (int i = 0; i < num; i++) {
            trees.add(tree.makeNew());
            numberOfTrees.get(tree.getTreeType()).inc();
        }
    }
    public void fertilizeTrees(Tree needTree, int num) {
        for (Tree tree : trees) {
            if (money <= fertilizePrice || num == 0) break;

            if (tree.getTreeType().equals(needTree.getTreeType()) && !tree.isFertilized()) {
                tree.fertilize();
                money -= fertilizePrice;
                num--;
            }
        }
    }
    public void sellHarvest() {
        for (Tree tree : trees) {
            money += tree.soldHarvest();
        }
    }
    public void uproot() {
        ArrayList<Tree> tmp = new ArrayList<>();
        for (Tree tree : trees) {
            if (!tree.isOld()) {
                tmp.add(tree);
            }
        }
        trees = tmp;
    }

    public int getFreeSpace() { return gardenSize - trees.size(); }
    public int getGardenSize() { return gardenSize; }
    public int getMoney() {
        return money;
    }
    public int getFertilizePrice() {
        return fertilizePrice;
    }

    public Map<Tree, TreesCounter> getNumberOfTrees() {
        for (TreesCounter counter : map.values()) {
            counter.reset();
        }
        for (Tree tree : map.keySet()) {
            map.get(tree).add(numberOfTrees.get(tree.getTreeType()).getCount());
        }
        return map;
    }

    public Map<Tree, TreesCounter> getNumberOfNotFertilizedTrees() {
        for (TreesCounter counter : map.values()) {
            counter.reset();
        }
        for (Tree tree : map.keySet()) {
            for (Tree gardenTree : trees) {
                if (gardenTree.isFruitful() && !gardenTree.isFertilized() && tree.getTreeType().equals(gardenTree.getTreeType())) {
                    map.get(tree).inc();
                }
            }
        }
        return map;
    }

    public Map<Tree, TreesCounter> getNumberOfOldTrees() {
        for (TreesCounter counter : map.values()) {
            counter.reset();
        }
        for (Tree tree : map.keySet()) {
            for (Tree gardenTree : trees) {
                if (tree.isOld() && tree.getTreeType().equals(gardenTree.getTreeType())) {
                    map.get(tree).inc();
                }
            }
        }
        return map;
    }

    public Map<Tree, TreesCounter> getTreesHarvestPrice() {
        for (TreesCounter counter : map.values()) {
            counter.reset();
        }
        for (Tree tree : map.keySet()) {
            for (Tree gardenTree : trees) {
                if (tree.getTreeType().equals(gardenTree.getTreeType())) {
                    map.get(tree).add(gardenTree.getHarvest());
                }
            }
        }

        return map;
    }

    public void nextSeason() {
        if (season instanceof SpringView) {
            season = summerView;
        }
        else if (season instanceof SummerView) {
            season = autumnView;
            for (Tree tree : trees) {
                tree.setHarvest();
            }
        }
        else if (season instanceof AutumnView) {
            season = winterView;
        }
        else if (season instanceof WinterView) {
            season = springView;
            for (Tree tree : trees) {
                tree.nextYear();
            }
        }
    }

    public SeasonView getSeason() {
        return season;
    }
}
