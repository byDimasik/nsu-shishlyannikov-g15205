package ru.nsu.fit.g15205.shishlyannikov.view;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;


public class GameFrame extends JFrame {
    public GameFrame() {
        super("Funny Garden");
        setPreferredSize(new Dimension(640, 520));
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void updateFrame(JPanel panel) {
        SwingUtilities.invokeLater( () -> {
            getContentPane().removeAll();

            getContentPane().add(panel);

            pack();
            repaint();
            setVisible(true);
        });
    }
}
