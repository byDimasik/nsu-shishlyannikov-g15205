package ru.nsu.fit.g15205.shishlyannikov.model.trees;

import ru.nsu.fit.g15205.shishlyannikov.model.Tree;

public class Cherry extends Tree {
    public Cherry() {
        super();
    }

    @Override
    public String getTreeType() {
        return "cherry";
    }

    @Override
    public String getTreeName() {
        return "Вишня";
    }
}
