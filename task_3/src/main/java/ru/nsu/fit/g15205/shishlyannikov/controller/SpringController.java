package ru.nsu.fit.g15205.shishlyannikov.controller;

import ru.nsu.fit.g15205.shishlyannikov.model.Garden;
import ru.nsu.fit.g15205.shishlyannikov.model.Tree;

public class SpringController {
    private Garden garden;

    public SpringController(Garden garden) {
        this.garden = garden;
    }
    public void plant(Tree tree, int num) {
        garden.addTrees(tree, num);
    }
}
