package ru.nsu.fit.g15205.shishlyannikov.model;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class TreesFactory {
    private Map<String, Tree> treeMap = new HashMap<>();
    public Tree getTree(String treeType) {
        if (treeMap.containsKey(treeType)) {
            return treeMap.get(treeType).makeNew();
        }

        try (InputStream inputStream = new FileInputStream("resources/trees_properties/"+treeType+".properties")) {
            Properties properties = new Properties();
            properties.load(inputStream);
            int growthTime = Integer.parseInt(properties.getProperty("GROWTH"));
            int fruitfulPeriod = Integer.parseInt(properties.getProperty("FRUITFUL"));
            int[] crop = {0, 0};
            crop[0] = Integer.parseInt(properties.getProperty("CROPSTART"));
            crop[1] = Integer.parseInt(properties.getProperty("CROPEND"));
            int price = Integer.parseInt(properties.getProperty("PRICE"));

            Class treeClass = Class.forName(properties.getProperty("TREE"));
            Tree tree = (Tree) treeClass.newInstance();
            tree.init(growthTime, fruitfulPeriod, crop, price);
            treeMap.put(treeType, tree);
            return tree;
        } catch (IOException | NullPointerException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            System.err.println(ex.getLocalizedMessage());
        }
        return null;
    }

    public ArrayList<Tree> getAllTrees() {
        String[] treesTypes = {"apple", "cherry", "peach", "pear"};
        ArrayList<Tree> trees = new ArrayList<>();
        for (String tree : treesTypes) {
            trees.add(getTree(tree));
        }
        return trees;
    }
}
