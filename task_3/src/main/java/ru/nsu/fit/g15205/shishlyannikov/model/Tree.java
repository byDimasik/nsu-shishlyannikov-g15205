package ru.nsu.fit.g15205.shishlyannikov.model;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Tree {
    private int age = 0;
    private int growthTime;
    private int fruitfulPeriod;
    private ArrayList<Integer> crop = new ArrayList<>();
    private int price;
    private boolean fruitful = false;
    private boolean fertilized = false;
    private int harvest;
    private final int fertilizerEffect = 20;

    public void init(int growthTime, int fruitfulPeriod, int[] crop, int price) {
        this.growthTime = growthTime;
        this.fruitfulPeriod = fruitfulPeriod;
        this.crop.add(crop[0]);
        this.crop.add(crop[1]);
        this.price = price;
    }

    public void setHarvest() {
        if (isFruitful()) {
            harvest = ThreadLocalRandom.current().nextInt(crop.get(0), crop.get(1) + 1);
            harvest = harvest + (harvest / 100 * fertilizerEffect) * (fertilized ? 1 : 0);
        }
        else {
            harvest = 0;
        }
    }

    public void nextYear() {
        age++;
        fertilized = false;
        fruitful = (age >= growthTime && age <= fruitfulPeriod);
    }

    public int getHarvest() {
        return harvest * price;
    }

    public int soldHarvest() {
        int sold = price * harvest;
        harvest = 0;
        return sold;
    }

    public boolean isFruitful() {
        return fruitful;
    }
    public boolean isFertilized() {
        return fertilized;
    }
    public boolean isOld() {
        return age > fruitfulPeriod;
    }

    public void fertilize() {
        fertilized = true;
    }

    public Tree makeNew() {
        Class<? extends Tree> treeClass = this.getClass();
        Tree newTree = null;
        try {
            newTree = treeClass.newInstance();
            int[] crop = {this.crop.get(0), this.crop.get(1)};
            newTree.init(growthTime, fruitfulPeriod, crop, price);
        } catch (InstantiationException | IllegalAccessException ex) {
            System.err.println(ex.getLocalizedMessage());
        }
        return newTree;
    }

    public String getTreeType() {
        return "tree";
    }

    public String getTreeName() {
        return "дерево";
    }
}
