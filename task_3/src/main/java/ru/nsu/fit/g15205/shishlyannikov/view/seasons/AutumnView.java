package ru.nsu.fit.g15205.shishlyannikov.view.seasons;

import ru.nsu.fit.g15205.shishlyannikov.controller.AutumnController;
import ru.nsu.fit.g15205.shishlyannikov.controller.GameController;
import ru.nsu.fit.g15205.shishlyannikov.controller.SummerController;
import ru.nsu.fit.g15205.shishlyannikov.model.Garden;
import ru.nsu.fit.g15205.shishlyannikov.model.Tree;
import ru.nsu.fit.g15205.shishlyannikov.model.TreesCounter;
import ru.nsu.fit.g15205.shishlyannikov.view.GameButton;
import ru.nsu.fit.g15205.shishlyannikov.view.SeasonView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class AutumnView implements SeasonView {
    private JPanel jPanel;
    private AutumnController autumnController;
    private Garden garden;
    private GameController gameController;

    public AutumnView(AutumnController autumnController, Garden garden, GameController gameController) {
        this.autumnController = autumnController;
        this.garden = garden;
        this.gameController = gameController;
    }

    public JPanel seasonComing() {
        Map<Tree, TreesCounter> trees = garden.getTreesHarvestPrice();
        jPanel = new JPanel(new GridLayout(trees.keySet().size()+1+1, 1));
        jPanel.setBackground(new Color(252, 246, 158));

        JLabel money = new JLabel("Всего денег: " + garden.getMoney());
        jPanel.add(money);

        JLabel treeLabel;
        GameButton button;
        for (Tree tree : trees.keySet()) {
            treeLabel = new JLabel(tree.getTreeName() + ": " + trees.get(tree).getCount() + " р.");

            jPanel.add(treeLabel);
        }

        button = new GameButton("Продать!", "autumn");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                autumnController.sellHarvest();

                gameController.updateSeason();
            }
        });
        jPanel.add(button);

        return jPanel;
    }

    public Color getColor() {
        return new Color(252, 246, 158);
    }
}
