package ru.nsu.fit.g15205.shishlyannikov.view;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class GameButton extends JButton {
    private ArrayList<ImageIcon> icons = new ArrayList<>();

    public GameButton(String text, String season) {
        super(text);

        class GameButtonMouseListener implements MouseListener {
            public void mouseClicked(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
                changeIcon(1);
            }

            public void mouseExited(MouseEvent e) {
                changeIcon(0);
            }

            public void mousePressed(MouseEvent e) {
                changeIcon(2);
            }

            public void mouseReleased(MouseEvent e) {
                changeIcon(0);
            }
        }
        addMouseListener(new GameButtonMouseListener());

        addIcon("resources/images/"+ season +"/Par.png");
        addIcon("resources/images/"+ season +"/Cursor.png");
        addIcon("resources/images/"+ season +"/Click.png");

        setIcon(icons.get(0));
        setHorizontalTextPosition(SwingConstants.CENTER);

        Border emptyBorder = BorderFactory.createEmptyBorder();
        setBorder(emptyBorder);
        repaint();
    }

    private void addIcon(String path) {
        try {
            Image image = ImageIO.read(new File(path));
            image = image.getScaledInstance(150, 40, Image.SCALE_DEFAULT);
            icons.add(new ImageIcon(image));
        } catch (IOException ex) {
            System.err.println(path + " " + ex.getLocalizedMessage());
        }
    }

    public void changeIcon(int imageIndex) {
        setIcon(icons.get(imageIndex));
    }
}