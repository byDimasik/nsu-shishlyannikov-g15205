package ru.nsu.fit.g15205.shishlyannikov.controller;

import ru.nsu.fit.g15205.shishlyannikov.model.Garden;
import ru.nsu.fit.g15205.shishlyannikov.model.Tree;

public class SummerController {
    private Garden garden;

    public SummerController(Garden garden) {
        this.garden = garden;
    }
    public void fertilize(Tree tree, int num) {
        if (num > garden.getNumberOfNotFertilizedTrees().get(tree).getCount()) {
            num = garden.getNumberOfNotFertilizedTrees().get(tree).getCount();
        }
        garden.fertilizeTrees(tree, num);
    }
}
