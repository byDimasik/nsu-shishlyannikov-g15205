package ru.nsu.fit.g15205.shishlyannikov.view;

import javax.swing.*;
import java.awt.*;

public interface SeasonView {
    JPanel seasonComing();
    Color getColor();
}
