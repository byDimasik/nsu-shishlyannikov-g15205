package ru.nsu.fit.g15205.shishlyannikov.view.seasons;

import ru.nsu.fit.g15205.shishlyannikov.controller.GameController;
import ru.nsu.fit.g15205.shishlyannikov.controller.WinterController;
import ru.nsu.fit.g15205.shishlyannikov.model.Garden;
import ru.nsu.fit.g15205.shishlyannikov.model.Tree;
import ru.nsu.fit.g15205.shishlyannikov.model.TreesCounter;
import ru.nsu.fit.g15205.shishlyannikov.view.GameButton;
import ru.nsu.fit.g15205.shishlyannikov.view.SeasonView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

public class WinterView  implements SeasonView {
    private JPanel jPanel;
    private Garden garden;
    private WinterController winterController;
    private GameController gameController;

    public WinterView(WinterController winterController, Garden garden, GameController gameController) {
        this.winterController = winterController;
        this.garden = garden;
        this.gameController = gameController;
    }

    public JPanel seasonComing() {
        Map<Tree, TreesCounter> trees = garden.getNumberOfOldTrees();
        jPanel = new JPanel(new GridLayout(trees.keySet().size()+1, 1));
        jPanel.setBackground(new Color(213, 249, 245));

        JLabel treeLabel;
        GameButton button;
        for (Tree tree : trees.keySet()) {
            treeLabel = new JLabel(tree.getTreeName() + ": " + trees.get(tree).getCount());

            jPanel.add(treeLabel);
        }

        button = new GameButton("Выкорчевать!", "winter");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                winterController.uproot();

                gameController.updateSeason();
            }
        });
        jPanel.add(button);

        return jPanel;
    }

    public Color getColor() {
        return new Color(213, 249, 245);
    }
}
