package ru.nsu.fit.g15205.shishlyannikov.model.trees;

import ru.nsu.fit.g15205.shishlyannikov.model.Tree;

public class Pear extends Tree {
    public Pear() {
        super();
    }

    @Override
    public String getTreeType() {
        return "pear";
    }

    @Override
    public String getTreeName() {
        return "Груша";
    }
}
