package ru.nsu.fit.g15205.shishlyannikov.model.trees;

import ru.nsu.fit.g15205.shishlyannikov.model.Tree;

public class Peach extends Tree {
    public Peach() {
        super();
    }

    @Override
    public String getTreeType() {
        return "peach";
    }

    @Override
    public String getTreeName() {
        return "Персик";
    }
}
