package ru.nsu.fit.g15205.shishlyannikov;

import ru.nsu.fit.g15205.shishlyannikov.controller.GameController;
import ru.nsu.fit.g15205.shishlyannikov.model.Garden;
import ru.nsu.fit.g15205.shishlyannikov.view.MainMenu;
import ru.nsu.fit.g15205.shishlyannikov.view.GameFrame;
import ru.nsu.fit.g15205.shishlyannikov.view.GameView;

public class Main {
    public static void main(String[] args) {
        Garden garden = new Garden();
        GameFrame frame = new GameFrame();
        MainMenu mainMenu = new MainMenu();

        GameController gameController = new GameController(frame, garden, mainMenu);
        GameView gameView = new GameView(gameController, garden);

        mainMenu.addController(gameController);
        garden.addGameController(gameController);

        gameController.backToMenu();
    }
}