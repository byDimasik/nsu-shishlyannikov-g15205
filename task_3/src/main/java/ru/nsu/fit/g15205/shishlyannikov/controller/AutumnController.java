package ru.nsu.fit.g15205.shishlyannikov.controller;

import ru.nsu.fit.g15205.shishlyannikov.model.Garden;

public class AutumnController {
    private Garden garden;

    public AutumnController(Garden garden) {
        this.garden = garden;
    }
    public void sellHarvest() {
        garden.sellHarvest();
    }
}
