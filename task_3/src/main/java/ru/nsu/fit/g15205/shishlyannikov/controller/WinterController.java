package ru.nsu.fit.g15205.shishlyannikov.controller;

import ru.nsu.fit.g15205.shishlyannikov.model.Garden;

public class WinterController {
    private Garden garden;

    public WinterController(Garden garden) {
        this.garden = garden;
    }
    public void uproot() {
        garden.uproot();
    }
}
