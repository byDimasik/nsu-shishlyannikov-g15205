package ru.nsu.fit.g15205.shishlyannikov.view.seasons;

import ru.nsu.fit.g15205.shishlyannikov.controller.GameController;
import ru.nsu.fit.g15205.shishlyannikov.controller.SpringController;
import ru.nsu.fit.g15205.shishlyannikov.controller.SummerController;
import ru.nsu.fit.g15205.shishlyannikov.model.Garden;
import ru.nsu.fit.g15205.shishlyannikov.model.Tree;
import ru.nsu.fit.g15205.shishlyannikov.model.TreesCounter;
import ru.nsu.fit.g15205.shishlyannikov.view.GameButton;
import ru.nsu.fit.g15205.shishlyannikov.view.SeasonView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

public class SummerView implements SeasonView {
    private JPanel jPanel;
    private JSlider slider;
    private SummerController summerController;
    private Garden garden;
    private GameController gameController;

    public SummerView(SummerController summerController, Garden garden, GameController gameController) {
        this.summerController = summerController;
        this.garden = garden;
        this.gameController = gameController;
    }

    public JPanel seasonComing() {
        Map<Tree, TreesCounter> trees = garden.getNumberOfNotFertilizedTrees();
        jPanel = new JPanel(new GridLayout(trees.keySet().size()+1+1, 2));
        jPanel.setBackground(new Color(212, 255, 202));

        slider = new JSlider(0, garden.getGardenSize());
        slider.setValue(1);
        slider.setMinorTickSpacing(1);
        slider.setMajorTickSpacing(5);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);

        JLabel money = new JLabel("Всего денег: " + garden.getMoney());
        jPanel.add(money);
        jPanel.add(new JLabel(" Цена одного удобрения: " + garden.getFertilizePrice()));
        JLabel treeLabel;
        JButton button;
        for (Tree tree : trees.keySet()) {
            treeLabel = new JLabel(tree.getTreeName() + ". Не удобрено: " + trees.get(tree).getCount());
            button = new GameButton("Удобрить!", "summer");
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    summerController.fertilize(tree, slider.getValue());

                    gameController.updateSeason();
                }
            });

            jPanel.add(treeLabel);
            jPanel.add(button);
        }
        jPanel.add(slider);

        return jPanel;
    }

    public Color getColor() {
        return new Color(212, 255, 202);
    }
}
