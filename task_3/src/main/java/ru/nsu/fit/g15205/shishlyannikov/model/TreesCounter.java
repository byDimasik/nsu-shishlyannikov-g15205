package ru.nsu.fit.g15205.shishlyannikov.model;

public class TreesCounter {
    private int count = 0;

    public void inc() { count++; }
    public int getCount() { return count; }
    public void add(int add) { count += add; }
    public void reset() { count = 0; }
}
