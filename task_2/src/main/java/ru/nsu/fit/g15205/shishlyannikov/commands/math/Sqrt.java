package ru.nsu.fit.g15205.shishlyannikov.commands.math;

import org.apache.log4j.Logger;
import ru.nsu.fit.g15205.shishlyannikov.commands.RootOfNegativeException;
import ru.nsu.fit.g15205.shishlyannikov.core.Command;
import ru.nsu.fit.g15205.shishlyannikov.core.Context;
import ru.nsu.fit.g15205.shishlyannikov.core.EmptyStackException;

import java.util.List;

public class Sqrt implements Command {
    private static final Logger logger = Logger.getLogger(Sqrt.class);
    public void execute(Context context, List<String> args) throws RootOfNegativeException, EmptyStackException {
        try {
            Double a = context.pop();

            if (a < 0.0) {
                context.push(a);
                throw new RootOfNegativeException("Sqrt error");
            }

            context.push(Math.sqrt(a));
            logger.debug("√" + a + " = " + context.peek());
        } catch (EmptyStackException ex) {
            throw new EmptyStackException("Sqrt error");
        }
    }
}
