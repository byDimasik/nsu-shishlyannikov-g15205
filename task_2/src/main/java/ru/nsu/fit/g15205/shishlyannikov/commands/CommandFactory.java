package ru.nsu.fit.g15205.shishlyannikov.commands;

import org.apache.log4j.Logger;
import ru.nsu.fit.g15205.shishlyannikov.core.AbstractCommandFactory;
import ru.nsu.fit.g15205.shishlyannikov.core.CalculatorException;
import ru.nsu.fit.g15205.shishlyannikov.core.Command;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class CommandFactory extends AbstractCommandFactory{
    private static final Logger logger = Logger.getLogger(CommandFactory.class);

    private String propFileName = "CommandFactory.properties";
    private Properties properties;
    private Map<String, Command> commandsObjects;

    public CommandFactory() throws NoFactoryPropertiesFileException {
        commandsObjects = new HashMap<>();
        try (InputStream inputStream = getClass().getResourceAsStream(propFileName)) {
            properties = new Properties();
            properties.load(inputStream);
        } catch (IOException | NullPointerException ex) {
            logger.fatal("Error while reading file: " + propFileName);
            throw new NoFactoryPropertiesFileException();
        }
    }
    public Command getCommand(String name) throws CalculatorException {
        Class commandClass;
        Command command;
        String command_name = name.toUpperCase();
        try {
            if (commandsObjects.containsKey(command_name)) {
                command = commandsObjects.get(command_name);
                logger.debug("Factory return command: " + name);
            }
            else {
                commandClass = Class.forName(properties.getProperty(command_name));
                command = (Command) commandClass.newInstance();
                commandsObjects.put(command_name, command);
                logger.debug("Factory create command: " + name);
            }
            return command;
        } catch (NullPointerException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            throw new CommandNotFoundException(name);
        }
    }
}
