package ru.nsu.fit.g15205.shishlyannikov;

import org.apache.log4j.Logger;
import ru.nsu.fit.g15205.shishlyannikov.commands.CommandFactory;
import ru.nsu.fit.g15205.shishlyannikov.commands.NoFactoryPropertiesFileException;
import ru.nsu.fit.g15205.shishlyannikov.core.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class StackCalculator
{
    private static final Logger logger = Logger.getLogger(StackCalculator.class);

    public static void main( String[] args ) {
        try (CommandParser commandParser = new CommandParser(getInputStream(args))) {
            AbstractCommandFactory commandFactory = new CommandFactory();
            Context context = new Context();
            Executor executor = new Executor(commandFactory, context, commandParser);
            logger.info("Start calculating...");
            executor.calculate();
            logger.info("End calculating.");
        } catch (IOException | NoFactoryPropertiesFileException ex) {
            logger.fatal("", ex);
        }
    }
    
    private static InputStream getInputStream(String[] args) throws FileNotFoundException {
        InputStream inputStream = System.in;
        // если передан файл, считываем данные из него
        if (args.length == 1) {
            inputStream = new FileInputStream(args[0]);
        }
        return inputStream;
    }
}
