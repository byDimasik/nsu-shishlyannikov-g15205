package ru.nsu.fit.g15205.shishlyannikov.commands.stack;

import org.apache.log4j.Logger;
import ru.nsu.fit.g15205.shishlyannikov.commands.NoArgumentsException;
import ru.nsu.fit.g15205.shishlyannikov.core.UndefinedVariableException;
import ru.nsu.fit.g15205.shishlyannikov.core.Command;
import ru.nsu.fit.g15205.shishlyannikov.core.Context;

import java.util.List;

public class Push implements Command {
    private static final Logger logger = Logger.getLogger(Push.class);

    public void execute(Context context, List<String> args) throws UndefinedVariableException, NoArgumentsException {
        String arg;
        try {
            arg = args.get(0);
        } catch (IndexOutOfBoundsException ex) {
            throw new NoArgumentsException("Push error");
        }
        Double arg_d = isDouble(arg);
        if (arg_d != null) {
            context.push(arg_d);
            logger.debug("Push " + arg_d);
        }
        else
            try {
                context.push(context.getDefine(arg));
                logger.debug("Push " + context.getDefine(arg));
            } catch (UndefinedVariableException e) {
                throw new UndefinedVariableException("Push error: " + arg + " - is not exist");
            }
    }

    private Double isDouble(String str)
    {
        try {
            return Double.valueOf(str);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
