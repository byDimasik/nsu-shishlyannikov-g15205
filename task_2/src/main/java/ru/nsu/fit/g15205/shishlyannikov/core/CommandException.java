package ru.nsu.fit.g15205.shishlyannikov.core;

public class CommandException extends CalculatorException {
    public CommandException(String message)
    {
        super(message);
    }
}
