package ru.nsu.fit.g15205.shishlyannikov.core;

public class CalculatorException extends Exception {
    public CalculatorException()
    {
        super();
    }

    public CalculatorException(String message)
    {
        super(message);
    }

    public CalculatorException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public CalculatorException(Throwable cause)
    {
        super(cause);
    }
}
