package ru.nsu.fit.g15205.shishlyannikov.core;

import java.util.*;

public class Context {
    private Map<String, Double> defines;
    private List<Double> stack;

    public Context() {
        defines = new HashMap<>();
        stack = new ArrayList<>();
    }

    public void push(Double num) {
        stack.add(num);
    }

    public Double pop() throws EmptyStackException {
        if (!stack.isEmpty()) {
            return stack.remove(stack.size() - 1);
        }
        else {
            throw new EmptyStackException();
        }
    }

    public Double peek() throws EmptyStackException {
        if (!stack.isEmpty()) {
            return stack.get(stack.size() - 1);
        }
        else {
            throw new EmptyStackException();
        }
    }

    public void setDefine(String key, Double value) {
        defines.put(key, value);
    }

    public Double getDefine(String key) throws UndefinedVariableException {
        if (!defines.containsKey(key))
            throw new UndefinedVariableException();

        return defines.get(key);
    }
}
