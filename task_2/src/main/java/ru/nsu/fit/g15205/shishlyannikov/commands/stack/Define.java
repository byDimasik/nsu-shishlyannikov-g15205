package ru.nsu.fit.g15205.shishlyannikov.commands.stack;

import org.apache.log4j.Logger;
import ru.nsu.fit.g15205.shishlyannikov.commands.NoArgumentsException;
import ru.nsu.fit.g15205.shishlyannikov.commands.WrongArgumentsTypesException;
import ru.nsu.fit.g15205.shishlyannikov.core.Command;
import ru.nsu.fit.g15205.shishlyannikov.core.Context;

import java.util.List;

public class Define implements Command {
    private static final Logger logger = Logger.getLogger(Define.class);
    public void execute(Context context, List<String> args) throws NoArgumentsException, WrongArgumentsTypesException {
        try {
            context.setDefine(args.get(0), Double.valueOf(args.get(1)));
            logger.debug(args.get(0) + " = " + args.get(1));
        } catch (IndexOutOfBoundsException ex) {
            throw new NoArgumentsException("Define error");
        } catch (NumberFormatException ex) {
            throw new WrongArgumentsTypesException("Define error");
        }
    }
}
