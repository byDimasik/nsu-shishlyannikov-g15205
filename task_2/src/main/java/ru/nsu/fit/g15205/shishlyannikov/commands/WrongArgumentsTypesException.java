package ru.nsu.fit.g15205.shishlyannikov.commands;

import ru.nsu.fit.g15205.shishlyannikov.core.CalculatorException;

public class WrongArgumentsTypesException extends CalculatorException {
    public WrongArgumentsTypesException(String message)
    {
        super(message + ": Wrong arguments");
    }
}
