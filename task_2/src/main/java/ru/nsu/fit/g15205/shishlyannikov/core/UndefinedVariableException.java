package ru.nsu.fit.g15205.shishlyannikov.core;

public class UndefinedVariableException extends CalculatorException {
    public UndefinedVariableException()
    {
        super();
    }
    public UndefinedVariableException(String message)
    {
        super(message + ": Undefined Variable");
    }
}