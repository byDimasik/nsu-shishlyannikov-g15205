package ru.nsu.fit.g15205.shishlyannikov.core;

/**
* Фрабрика команд
 * Загружает классы команд в соответствии с конфигурацией в файле, имя которого задается в propFileName
 * getCommand() возвращает объект класса команды соответствующий принимаемой строке command_name
* */
public abstract class AbstractCommandFactory {
    public abstract Command getCommand(String command_name) throws CalculatorException;
}
