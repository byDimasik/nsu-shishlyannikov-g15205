package ru.nsu.fit.g15205.shishlyannikov.core;

import java.util.List;

public interface Command {
    void execute(Context context, List<String> args) throws CalculatorException;
}