package ru.nsu.fit.g15205.shishlyannikov.commands;
import ru.nsu.fit.g15205.shishlyannikov.core.CalculatorException;

public class CommandNotFoundException extends CalculatorException {
    public CommandNotFoundException(String message)
    {
        super(message + ": Command not found");
    }
}