package ru.nsu.fit.g15205.shishlyannikov.commands.math;

import org.apache.log4j.Logger;
import ru.nsu.fit.g15205.shishlyannikov.core.Command;
import ru.nsu.fit.g15205.shishlyannikov.core.Context;
import ru.nsu.fit.g15205.shishlyannikov.core.EmptyStackException;

import java.util.List;

public class Div implements Command {
    private static final Logger logger = Logger.getLogger(Div.class);
    public void execute(Context context, List<String> args) throws EmptyStackException {
        Double a, b = null;
        try {
            b = context.pop();
            a = context.pop();

            context.push(a / b);
            logger.debug(a + " / " + b + " = " + context.peek());
        } catch (EmptyStackException ex) {
            if (b != null) context.push(b);
            throw new EmptyStackException("Div error");
        }
    }
}
