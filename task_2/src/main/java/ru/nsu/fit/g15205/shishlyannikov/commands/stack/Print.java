package ru.nsu.fit.g15205.shishlyannikov.commands.stack;

import org.apache.log4j.Logger;
import ru.nsu.fit.g15205.shishlyannikov.core.EmptyStackException;
import ru.nsu.fit.g15205.shishlyannikov.core.Command;
import ru.nsu.fit.g15205.shishlyannikov.core.Context;

import java.util.List;

public class Print implements Command {
    private static final Logger logger = Logger.getLogger(Print.class);
    public void execute(Context context, List<String> args) throws EmptyStackException {
        try {
            System.out.println("Stack Top: " + context.peek());
            logger.debug("Print " + context.peek());
        } catch (EmptyStackException ex) {
            throw new EmptyStackException("Print error");
        }
    }
}
