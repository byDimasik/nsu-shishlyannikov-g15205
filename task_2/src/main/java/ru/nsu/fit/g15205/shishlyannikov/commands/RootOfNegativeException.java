package ru.nsu.fit.g15205.shishlyannikov.commands;

import ru.nsu.fit.g15205.shishlyannikov.core.CommandException;

public class RootOfNegativeException extends CommandException {
    public RootOfNegativeException(String message)
    {
        super(message + ": Root of negative number");
    }
}
