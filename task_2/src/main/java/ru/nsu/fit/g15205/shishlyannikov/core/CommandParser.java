package ru.nsu.fit.g15205.shishlyannikov.core;

import org.apache.log4j.Logger;

import java.io.InputStream;
import java.util.Scanner;

public class CommandParser implements AutoCloseable {
    private static final Logger logger = Logger.getLogger(CommandParser.class);
    private Scanner scanner;

    public CommandParser(InputStream inputStream) {
        logger.debug("Open input stream");
        scanner = new Scanner(inputStream);
    }

    public String getCommand() {
        while (scanner.hasNextLine()){
            String command = scanner.nextLine();

            if (!command.equals("") && !command.startsWith("#")) {// пропускаем пустые строки и строки-комментарии
                logger.debug("Read command line: " + command);
                return command;
            }
        }
        return null;
    }

    public void close() {
        logger.debug("Close input stream");
        scanner.close();
    }
}
