package ru.nsu.fit.g15205.shishlyannikov.commands.math;

import org.apache.log4j.Logger;
import ru.nsu.fit.g15205.shishlyannikov.core.Command;
import ru.nsu.fit.g15205.shishlyannikov.core.Context;
import ru.nsu.fit.g15205.shishlyannikov.core.EmptyStackException;

import java.util.List;

public class Mul implements Command {
    private static final Logger logger = Logger.getLogger(Mul.class);

    public void execute(Context context, List<String> args) throws EmptyStackException {
        Double a = null , b;
        try {
            a = context.pop();
            b = context.pop();

            context.push(a * b);
            logger.debug(a + " * " + b + " = " + context.peek());
        } catch (EmptyStackException ex) {
            if (a != null) context.push(a);
            throw new EmptyStackException("Mul error");
        }
    }
}
