package ru.nsu.fit.g15205.shishlyannikov.core;

public class EmptyStackException extends CalculatorException {
    public EmptyStackException()
    {
        super();
    }
    public EmptyStackException(String message)
    {
        super(message + ": Empty Stack");
    }
}