package ru.nsu.fit.g15205.shishlyannikov.core;

import org.apache.log4j.Logger;
import ru.nsu.fit.g15205.shishlyannikov.commands.CommandFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Executor {
    private static final Logger logger = Logger.getLogger(Executor.class);
    private AbstractCommandFactory commandFactory;
    private Context context;
    private CommandParser commandParser;

    public Executor(AbstractCommandFactory cf, Context con, CommandParser cp) {
        commandFactory = cf;
        context = con;
        commandParser = cp;
    }

    public void calculate() {
        String line;

        while ((line = commandParser.getCommand()) != null && !"END".equals(line.toUpperCase()) ) {
            try {
                String[] cmd = line.split(" ");
                Command command = commandFactory.getCommand(cmd[0]);
                logger.debug("Execute command: " + cmd[0]);
                List<String> cmd_args = Arrays.asList(cmd).subList(1, cmd.length);

                command.execute(context, cmd_args);

            } catch (CalculatorException ex) {
                logger.warn(ex.getMessage());
            }
        }
    }
}
