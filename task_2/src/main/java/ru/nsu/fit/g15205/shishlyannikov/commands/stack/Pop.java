package ru.nsu.fit.g15205.shishlyannikov.commands.stack;

import org.apache.log4j.Logger;
import ru.nsu.fit.g15205.shishlyannikov.core.EmptyStackException;
import ru.nsu.fit.g15205.shishlyannikov.core.Command;
import ru.nsu.fit.g15205.shishlyannikov.core.Context;

import java.util.List;

public class Pop implements Command {
    private static final Logger logger = Logger.getLogger(Pop.class);
    public void execute(Context context, List<String> args) throws EmptyStackException {
        try {
            Double a = context.pop();
            logger.debug("Pop " + a);
        } catch (EmptyStackException ex) {
            throw new EmptyStackException("Pop error");
        }
    }
}
