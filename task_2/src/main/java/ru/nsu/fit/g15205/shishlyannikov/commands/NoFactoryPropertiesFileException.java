package ru.nsu.fit.g15205.shishlyannikov.commands;

import ru.nsu.fit.g15205.shishlyannikov.core.CalculatorException;

public class NoFactoryPropertiesFileException extends CalculatorException {
    public NoFactoryPropertiesFileException()
    {
        super();
    }
}
