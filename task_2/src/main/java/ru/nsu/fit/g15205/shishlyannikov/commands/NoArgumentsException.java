package ru.nsu.fit.g15205.shishlyannikov.commands;
import ru.nsu.fit.g15205.shishlyannikov.core.CommandException;

public class NoArgumentsException extends CommandException {
    public NoArgumentsException(String message)
    {
        super(message + ": No arguments");
    }
}