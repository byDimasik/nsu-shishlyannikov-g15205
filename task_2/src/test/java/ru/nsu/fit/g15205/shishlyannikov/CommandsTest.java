package ru.nsu.fit.g15205.shishlyannikov;

import junit.framework.TestCase;
import ru.nsu.fit.g15205.shishlyannikov.commands.math.*;
import ru.nsu.fit.g15205.shishlyannikov.commands.stack.Define;
import ru.nsu.fit.g15205.shishlyannikov.commands.stack.Pop;
import ru.nsu.fit.g15205.shishlyannikov.commands.stack.Push;
import ru.nsu.fit.g15205.shishlyannikov.core.*;

import java.util.ArrayList;
import java.util.List;

public class CommandsTest extends TestCase {
    private Double a = 5.0;
    private Double b = 20.0;
    private Context context;
    private Command command;


    @Override
    public void setUp() throws Exception {
        super.setUp();

        context = new Context();
        context.push(a);
        context.push(b);
    }

    public void testSum() throws CalculatorException {
        command = new Sum();

        command.execute(context, null);
        assertEquals(a + b, context.peek());

    }

    public void testSub() throws CalculatorException {
        command = new Sub();

        command.execute(context, null);
        assertEquals(a - b, context.peek());
    }

    public void testMul() throws CalculatorException {
        command = new Mul();

        command.execute(context, null);
        assertEquals(a * b, context.peek());
    }

    public void testDiv() throws CalculatorException {
        command = new Div();

        command.execute(context, null);
        assertEquals(a / b, context.peek());
    }

    public void testSqrt() throws CalculatorException {
        command = new Sqrt();

        command.execute(context, null);
        assertEquals(Math.sqrt(b), context.peek());
    }

    public void testPush() throws CalculatorException {
        command = new Push();
        List<String> list = new ArrayList<>();
        list.add("10");

        command.execute(context, list);
        assertEquals(10.0, context.peek());
    }

    public void testPop() throws CalculatorException {
        command = new Pop();

        command.execute(context, null);
        assertEquals(a, context.peek());
    }

    public void testDefine() throws CalculatorException {
        command = new Define();
        List<String> list= new ArrayList<>();
        list.add("ZERO");
        list.add("0");

        command.execute(context, list);
        assertEquals(0.0, context.getDefine("ZERO"));
    }
}
