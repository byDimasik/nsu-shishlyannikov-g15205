package ru.nsu.fit.g15205.shishlyannikov;

import junit.framework.TestCase;
import ru.nsu.fit.g15205.shishlyannikov.commands.CommandFactory;
import ru.nsu.fit.g15205.shishlyannikov.commands.CommandNotFoundException;
import ru.nsu.fit.g15205.shishlyannikov.commands.NoFactoryPropertiesFileException;
import ru.nsu.fit.g15205.shishlyannikov.commands.math.*;
import ru.nsu.fit.g15205.shishlyannikov.commands.stack.Define;
import ru.nsu.fit.g15205.shishlyannikov.commands.stack.Pop;
import ru.nsu.fit.g15205.shishlyannikov.commands.stack.Print;
import ru.nsu.fit.g15205.shishlyannikov.commands.stack.Push;
import ru.nsu.fit.g15205.shishlyannikov.core.AbstractCommandFactory;
import ru.nsu.fit.g15205.shishlyannikov.core.CalculatorException;
import ru.nsu.fit.g15205.shishlyannikov.core.Command;

public class FactoryTest extends TestCase {
    private AbstractCommandFactory factory;

    public void testFactory() throws CalculatorException {
        factory = new CommandFactory();

        Command command = factory.getCommand("push");
        assertTrue(command instanceof Push);
        command = factory.getCommand("pop");
        assertTrue(command instanceof Pop);
        command = factory.getCommand("define");
        assertTrue(command instanceof Define);
        command = factory.getCommand("print");
        assertTrue(command instanceof Print);

        command = factory.getCommand("/");
        assertTrue(command instanceof Div);
        command = factory.getCommand("*");
        assertTrue(command instanceof Mul);
        command = factory.getCommand("sqrt");
        assertTrue(command instanceof Sqrt);
        command = factory.getCommand("-");
        assertTrue(command instanceof Sub);
        command = factory.getCommand("+");
        assertTrue(command instanceof Sum);
    }

    public void testNoCommand() {
        try {
            factory = new CommandFactory();
        } catch (NoFactoryPropertiesFileException ex) {
            return;
        }
        try {
            factory.getCommand("not command");
            fail("Expected CommandNotFoundException to be thrown");
        } catch (CalculatorException ex) {
            assert ex instanceof CommandNotFoundException;
        }
    }
}
